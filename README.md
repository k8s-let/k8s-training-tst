# Kubernetes Training Repository

This repository is read by ArgoCD. All ArgoCD Application objects
in /argocd are evaluated and applied to the *tst* cluster. Synchronization
is set to *automated*, changes are active within a minute.

The branch *master* is protected:

 * *push* operation is disabled
 * *merge* is enabled for everyone

To add changes, you first have to create a merge request.
